#!/usr/bin/env python3

import subprocess
import argparse
import re

MAC_ADDRESS_RE = r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w"

def parse_args():
    parser = argparse.ArgumentParser(description='Changer MAC address of a given interace')
    parser.add_argument('--iface', '-i', nargs=1, required=True, type=str,
                        metavar='INTERFACE', dest='interface',
                        help='Interface whose MAC address needs to be changed')
    parser.add_argument('--mac', '-m', nargs=1, required=True, type=str,
                        metavar='NEW_MAC', dest='new_mac',
                        help='New MAC address')

    args = parser.parse_args()
    iface = args.interface[0]
    new_mac = args.new_mac[0]

    return iface, new_mac

def parse_mac_addr_ifconfig(ifconfig_out):
    parsed_mac = re.search(MAC_ADDRESS_RE, str(ifconfig_out))
    if (parsed_mac):
        return parsed_mac.group(0)
    else:
        return str(None)

def check_valid_iface(iface):
    try:
        ifconfig_out = subprocess.check_output(["ifconfig", iface],
                                               stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as exc:
        print("[-] " + iface + " does not exist")
        exit(1)

    if (parse_mac_addr_ifconfig(ifconfig_out) == str(None)):
        print("[-] " + iface + " does not have a MAC address !")
        exit(1)

def check_valid_mac(mac):
    if not re.match(MAC_ADDRESS_RE, new_mac):
       print("[-] Requested MAC address is invalid: " + new_mac)
       exit(1)

def change_mac(iface, new_mac):
    print("[+] Changing MAC address for " + iface + " to " + new_mac)

    subprocess.call(["ifconfig", iface, "down"])
    subprocess.call(["ifconfig", iface, "hw", "ether", new_mac])
    subprocess.call(["ifconfig", iface, "up"])

def get_current_mac(iface):
    ifconfig_out = subprocess.check_output(["ifconfig", iface])

    return parse_mac_addr_ifconfig(ifconfig_out)

def check_new_mac(iface, new_mac):
    current_mac = get_current_mac(iface)
    if current_mac != new_mac:
        print("[-] MAC address has not been changed !")
    else:
        print("[+] MAC address has been changed successfully !")

# Parse user args
(iface, new_mac) = parse_args()

# Check if requested MAC address is valid
check_valid_mac(new_mac)

# Check that iface is valid and has a MAC address
check_valid_iface(iface)

# Get current MAC address
current_mac = get_current_mac(iface)
print("[+] Current MAC address: " + current_mac)

# Change iface's MAC address
change_mac(iface, new_mac)

# Check that the new MAC address is the one requested by the user
check_new_mac(iface, new_mac)
